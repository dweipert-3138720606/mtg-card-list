<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name', 144);
            $table->string('printing', 10);
            $table->string('condition', 2);
            $table->string('language', 30);
            $table->integer('quantity');

            $table->string('foil', 4);
            $table->boolean('signed')->default(false);
            $table->boolean('alter')->default(false);

            $table->integer('multiverseId');
            $table->text('scryfallId');
            $table->string('matchedName', 144);

            $table->unique(['name', 'printing', 'condition', 'language', 'foil', 'alter', 'signed', 'multiverseId'], 'inventory_unique');
            #$table->unique(['name', 'printing', 'condition', 'language'], 'inventory_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory');
    }
}
