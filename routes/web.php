<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('db/delete', function () {
    \Illuminate\Support\Facades\DB::table('mcm_stock')->delete();
    \Illuminate\Support\Facades\DB::table('inventory')->delete();
    return back()->with('msg', 'Deleted inventory!');
});

// Inventory Management Services
Route::prefix('ims')->group(function () {
    Route::prefix('tappedout')->group(function () {
        Route::get('import', fn () => view('inventory-management-services.tappedout.import'))->name('ims.tappedout.import');
        Route::post('import', 'InventoryManagementServices\TappedOut\ImportController@import')->name('ims.tappedout.import');

        Route::get('export', 'InventoryManagementServices\TappedOut\ExportController@export')->name('ims.tappedout.export');
    });

    Route::prefix('cardmarket')->group(function () {
        Route::get('import', fn () => view('inventory-management-services.cardmarket.import'))->name('ims.cardmarket.import');
        Route::post('import', 'InventoryManagementServices\CardMarket\ApiController@import')->name('ims.cardmarket.import');
        Route::get('export', fn () => view('inventory-management-services.cardmarket.export'))->name('ims.cardmarket.export');
        Route::post('export', 'InventoryManagementServices\CardMarket\ApiController@export')->name('ims.cardmarket.export');

        Route::post('stock', 'InventoryManagementServices\CardMarket\ApiController@updateStock')->name('ims.cardmarket.stock');
        #Route::post('price', 'InventoryManagementServices\CardMarket\ApiController@updatePrice')->name('ims.cardmarket.price');
    });

    Route::prefix('csv')->group(function () {
        Route::get('import', fn () => view('inventory-management-services.csv.import'))->name('ims.csv.import');
        Route::post('import', 'InventoryManagementServices\CSV\ImportController@import')->name('ims.csv.import');
        Route::get('export', 'InventoryManagementServices\CSV\ExportController@export')->name('ims.csv.export');
    });
});
