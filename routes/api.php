<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('search/{name}', 'ApiController@search');
Route::post('add', 'ApiController@add');

Route::get('inventory/search/{name?}', 'ApiController@searchInventory');
Route::post('inventory/setQuantity', 'ApiController@setQuantity');
