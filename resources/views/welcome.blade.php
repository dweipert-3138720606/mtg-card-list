@extends('layout')

@section('head')
    <!-- jQuery -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
@stop

@section('body')
    <div id="app"></div>
@stop

@section('javascript')
    <script type="text/javascript">window.appSettings = {!! $jsSettings !!};</script>
    <script src="{{ mix('/js/vue.js') }}"></script>
@stop
