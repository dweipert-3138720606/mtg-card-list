<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>

    @yield('head')

    <!-- Bootstrap -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    @yield('style')
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
<div class="wrap">
    <header>
        <nav class="navbar navbar-expand-lg">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Home</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownImport" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Import
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdownImport">
                        <a class="dropdown-item" href="{{ route('ims.tappedout.import') }}">TappedOut</a>
                        <a class="dropdown-item" href="{{ route('ims.cardmarket.import') }}">Cardmarket</a>
                        <a class="dropdown-item" href="{{ route('ims.csv.import') }}">CSV</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownExport" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Export
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdownExport">
                        <a class="dropdown-item" href="{{ route('ims.tappedout.export') }}">TappedOut</a>
                        <a class="dropdown-item" href="{{ route('ims.cardmarket.export') }}">Cardmarket</a>
                        <a class="dropdown-item" href="{{ route('ims.csv.export') }}">CSV</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        DB Actions
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item js-confirm" href="{{ url('db/delete') }}">Delete everything!</a>
                    </div>
                </li>
            </ul>
        </nav>
    </header>

    @include('components.alert')
    @yield('body')
</div>

<script src="{{ mix('/js/app.js') }}"></script>
@yield('javascript')
</body>
</html>
