@extends('layout')

@section('body')
    <form action="{{ route('ims.tappedout.import') }}" method="post">
        @csrf
        <textarea name="import_list" id="" cols="30" rows="10" required style="width: 100%; height: 75%;"></textarea>
        <input type="submit" value="IMPORT">
    </form>
@stop
