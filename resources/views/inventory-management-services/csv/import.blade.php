@extends('layout')

@section('body')
    <form action="{{ route('ims.csv.import') }}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="file" name="csv">
        <input type="submit" value="IMPORT">
    </form>
@stop
