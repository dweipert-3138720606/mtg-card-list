@extends('layout')

@section('body')
    <form action="{{ route('ims.cardmarket.stock') }}" method="post">
        @csrf
        <input type="submit" value="UPDATE STOCK">
    </form>
    <form action="{{ route('ims.cardmarket.import') }}" method="post">
        @csrf
        <input type="submit" value="IMPORT">
    </form>
@stop
