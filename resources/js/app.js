/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

for (const el of document.querySelectorAll('.js-confirm')) {
    el.addEventListener('click', (ev) => {
        if (!confirm('Really do that?')) {
            ev.preventDefault();
        }
    });
}

// Bootstrap
window.$('.dropdown-toggle').dropdown();
