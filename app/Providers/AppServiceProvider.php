<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('welcome', function ($view) {
            $languages = DB::connection('mtgjson')->select('SELECT `language`, COUNT(*) as `count` FROM foreign_data GROUP BY `language` ORDER BY `count` DESC');
            $languages = array_filter($languages, fn ($item) => $item->count > 1);
            $languages = array_column($languages, 'language');
            array_unshift($languages, 'English');

            $sets = DB::connection('mtgjson')->select('SELECT `code`, `name`, `releaseDate` FROM `sets` GROUP BY `code` ORDER BY `releaseDate` ASC');
            $setsMapped = [];
            foreach ($sets as $set) {
                $setsMapped[$set->code] = $set->name;
            }

            $jsSettings = json_encode([
                'conditions' => getConditions(),
                'languages' => $languages,
                'sets' => $setsMapped,
                'cardCount' => [
                    'rows' => DB::table('inventory')->count(),
                    'unique' => count(DB::connection('mysql')->select('SELECT DISTINCT `name`, COUNT(*) as `count` FROM inventory GROUP BY `name`')),
                    'total' => intval(DB::table('inventory')->sum('quantity')),
                ],
            ]);
            $view->with(compact('jsSettings'));
        });
    }
}
