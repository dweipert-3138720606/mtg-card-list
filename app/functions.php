<?php

function getConditions()
{
    return [
        #'M' => 'Mint',
        'NM' => 'Near Mint',
        'EX' => 'Excellent',
        #'GD' => 'Good',
        'LP' => 'Light Played',
        'PL' => 'Played',
        'P' => 'Poor',
    ];
}

/**
 * TappedOut
 */

function getTappedOutConditionMap()
{
    return [
        'NM' => 'NM',
        'SL' => 'EX',
        'MP' => 'LP',
        'HP' => 'P',
    ];
}
function getFlippedTappedOutConditionMap()
{
    return [
        #'M' => 'NM',
        'NM' => 'NM',
        'EX' => 'SL',
        #'GD' => 'MP',
        'LP' => 'MP',
        'PL' => 'HP',
        'P' => 'HP',
    ];
}

function getTappedOutFoilMap()
{
    return [
        '' => '',
        'F' => 'foil',
        'PRE' => 'pre',
    ];
}

function getTappedOutLanguageMap()
{
    return [
        'EN' => 'English',
        'DE' => 'German',
        'FR' => 'French',
        'ES' => 'Spanish',
        'JA' => 'Japanese',
        'IT' => 'Italian'
    ];
}

function getTappedOutPromoSets()
{
    return [
        'PWP10', 'F13', 'PDP13', 'PM13', 'PM14', 'PM15',
        'PTHS', 'PZEN',
    ];
}

function getTappedOutSetMap()
{
    return [
        'NMS' => 'NEM', # Nemesis
    ];
}

/**
 * CardMarket
 */

function getCardMarketLanguageMap()
{
    return [
        1 => 'English',
        2 => 'French',
        3 => 'German',
        4 => 'Spanish',
        5 => 'Italian',
        6 => 'Simplified Chinese',
        7 => 'Japanese',
        8 => 'Protuguese',
        9 => 'Russian',
        10 => 'Korean',
        11 => 'Traditional Chinese',
    ];
}
