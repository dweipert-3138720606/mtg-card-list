<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function search (Request $request, string $name)
    {
        $name = strtolower($name);

        $sqlQueryWords = str_replace(' ', '%', $name);
        $matches = DB::connection('mtgjson')->select(
            "SELECT cards.name, cards.printings, cards.multiverseId, cards.scryfallId, cards.setCode, cards.rarity,
                    foreign_data.name as foreign_name, foreign_data.language, foreign_data.multiverseId as foreign_multiverseId
             FROM cards
             LEFT JOIN foreign_data
             ON cards.uuid = foreign_data.uuid
             WHERE cards.name LIKE '%$sqlQueryWords%' OR foreign_data.name LIKE '%$sqlQueryWords%'
             ORDER BY cards.name, foreign_data.language"
        );

        $grouped = [];
        foreach ($matches as $match) {
            // TODO: add api option to limit languages to search for
            // TODO: continue; if the language shouldn't be searched for

            $isForeign = $match->language !== 'English' && $match->language !== null;

            if (! isset($grouped[$match->name])) {
                #$matchedName = strpos(strtolower($match->name), $name) === false ? $match->foreign_name : $match->name;
                preg_match('@.*' . str_replace('%', '.*', $sqlQueryWords) . '.*@', strtolower($match->name), $nameMatches);
                $matchedName = ($nameMatches[0] ?? '') == strtolower($match->name) ? $match->name : $match->foreign_name;

                $grouped[$match->name] = [
                    'name' => $match->name,
                    'matchedName' => $matchedName,
                    'matchedLanguage' => $match->name == $matchedName ? 'English' : $match->language,
                    'names' => [],
                    'printings' => array_reverse(explode(',', $match->printings)),
                    'multiverseIds' => [],
                    'scryfallIds' => [],
                    'setRarities' => [],
                ];
            }

            $grouped[$match->name]['names'][$match->language] = $match->foreign_name;
            $grouped[$match->name]['multiverseIds'][$match->language ?: 'English'][$match->setCode] = $isForeign ? $match->foreign_multiverseId : $match->multiverseId;
            $grouped[$match->name]['scryfallIds'][$match->setCode] = $match->scryfallId;
            $grouped[$match->name]['setRarities'][$match->setCode] = [
                'common' => 'C',
                'uncommon' => 'U',
                'rare' => 'R',
                'mythic' => 'M',
            ][$match->rarity];

            // English fix
            if (! isset($grouped[$match->name]['multiverseIds']['English'][$match->setCode])) {
                $grouped[$match->name]['multiverseIds']['English'][$match->setCode] = $match->multiverseId;
            }
        }

        return $grouped;
    }

    public function add(Request $request)
    {
        $card = $request->get('card');

        $result = DB::statement(
            "INSERT INTO inventory
             (`name`, `printing`, `condition`, `language`, `foil`, `signed`, `alter`, `multiverseId`, `scryfallId`, `matchedName`, `quantity`)
             VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
             ON DUPLICATE KEY UPDATE `quantity` = `quantity` + ?",
            [
                $card['name'], $card['printing'], $card['condition'], $card['language'],
                $card['foil'] ?: '', 0, 0,
                $card['multiverseId'] ?? 0, $card['scryfallId'] ?? '000', $card['matchedName'], $card['quantity'], $card['quantity']
            ]
        );

        return $result;
    }

    public function searchInventory(Request $request, string $name = null)
    {
        $sqlQueryWords = str_replace(' ', '%', $name);
        $inventoryQuery = DB::table('inventory')->select();
        if ($name) {
            $inventoryQuery
                ->where('name', 'like', "%$sqlQueryWords%", 'or')
                ->where('matchedName', 'like', "%$sqlQueryWords%", 'or');
        }
        $inventory = $inventoryQuery
            ->orderBy('name')
            ->paginate($request->get('perPage', 15));

        // TODO: also search for foreign names but display them greyed-out if the card in another language is in the inventory

        return $inventory;
    }

    public function setQuantity(Request $request)
    {
        $card = $request->get('card');
        $quantity = $request->get('quantity');

        return DB::table('inventory')
            ->where('id', $card['id'])
            ->update([
                'quantity' => $quantity,
            ]);
    }
}
