<?php

namespace App\Http\Controllers\InventoryManagementServices\CSV;

use App\Http\Controllers\Controller;
use App\Http\Controllers\InventoryManagementServices\ExportInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExportController extends Controller implements ExportInterface
{
    public function export(Request $request)
    {
        $inventory = DB::table('inventory')->get([
            'name', 'printing', 'condition', 'language', 'quantity', 'foil', 'signed', 'alter',
            'multiverseId', 'scryfallId', 'matchedName',
        ])->toArray();

        # header line
        array_unshift($inventory, array_keys((array)$inventory[0]));

        $callback = function () use ($inventory) {
            $handle = fopen('php://output', 'w');
            foreach ($inventory as $item) {
                fputcsv($handle, (array)$item);
            }
            fclose($handle);
        };

        return response()->streamDownload($callback, 'inventory.csv');
    }
}
