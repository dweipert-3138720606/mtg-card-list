<?php

namespace App\Http\Controllers\InventoryManagementServices\CSV;

use App\Http\Controllers\Controller;
use App\Http\Controllers\InventoryManagementServices\ImportInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImportController extends Controller implements ImportInterface
{
    public function import(Request $request)
    {
        $csv = $request->file('csv');

        $handle = fopen($csv, 'r');
        $header = fgetcsv($handle);
        $headerIdx = array_flip($header);
        while ($row = fgetcsv($handle)) {
            DB::table('inventory')->updateOrInsert([
                'name' => $row[$headerIdx['name']],
                'printing' => $row[$headerIdx['printing']],
                'condition' => $row[$headerIdx['condition']],
                'language' => $row[$headerIdx['language']],
                'quantity' => $row[$headerIdx['quantity']],
                'foil' => $row[$headerIdx['foil']],
                'signed' => $row[$headerIdx['signed']],
                'alter' => $row[$headerIdx['alter']],
                'multiverseId' => $row[$headerIdx['multiverseId']],
                'scryfallId' => $row[$headerIdx['scryfallId']],
                'matchedName' => $row[$headerIdx['matchedName']],
            ]);
        }

        return back();
    }
}
