<?php

namespace App\Http\Controllers\InventoryManagementServices;

use Illuminate\Http\Request;

interface ImportInterface
{
    public function import(Request $request);
}
