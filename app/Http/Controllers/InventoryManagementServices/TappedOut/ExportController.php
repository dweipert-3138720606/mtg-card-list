<?php

namespace App\Http\Controllers\InventoryManagementServices\TappedOut;

use App\Http\Controllers\Controller;
use App\Http\Controllers\InventoryManagementServices\ExportInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExportController extends Controller implements ExportInterface
{
    public function export(Request $request)
    {
        $inventory = DB::table('inventory')->select()->get();
        $export = [];
        foreach ($inventory as $item) {
            $foil = array_flip(getTappedOutFoilMap())[$item->foil ?? ''];
            $language = $item->language != 'English' ? array_flip(getTappedOutLanguageMap())[$item->language] : '';
            $condition = $item->condition != 'NM' ? getFlippedTappedOutConditionMap()[$item->condition] : '';
            $printing = $this->convertToTappedOutPrinting($item->printing);
            $printing = $this->convertToTappedOutPromoPrinting($printing);

            $line = "{$item->quantity}x {$item->name} ($printing) *$foil* *$language* *$condition*";
            $line = str_replace(['**', '  '], ['', ' '], $line);
            $export[] = $line;
        }

        return view('inventory-management-services.tappedout.export')->with([
            'items' => $export,
        ]);
    }

    private function convertToTappedOutPromoPrinting($printing)
    {
        return in_array($printing, getTappedOutPromoSets()) ? '000' : $printing;
    }

    private function convertToTappedOutPrinting($printing)
    {
        return array_flip(getTappedOutSetMap())[$printing] ?? $printing;
    }
}
