<?php

namespace App\Http\Controllers\InventoryManagementServices\TappedOut;

use App\Http\Controllers\Controller;
use App\Http\Controllers\InventoryManagementServices\ImportInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ImportController extends Controller implements ImportInterface
{
    public function import(Request $request)
    {
        $list = $request->get('import_list');
        $list = explode("\r\n", $list);

        $parsed = [];
        $errors = [];
        foreach ($list as $item) {
            preg_match('/(\d+)x (.*) \((.*)\)( \*(\w+)\*)?( \*(\w+)\*)?( \*(\w+)\*)?/', $item, $matches);
            $parsed[] = $matches;

            $quantity = $matches[1];
            $name = $matches[2];
            $set = $matches[3];
            $set = getTappedOutSetMap()[$set] ?? $set;
            $identifiers = $this->getIdentifiers($matches);
            $foil = $this->getFoil($identifiers);
            $language = $this->getLanguage($identifiers);
            $condition = $this->getCondition($identifiers);

            $possibleSetCodes = [$set];
            if ($set == '000') {
                $possibleSetCodes = getTappedOutPromoSets();
            }
            $possibleSetCodesSql = implode('","', $possibleSetCodes);

            $card = null;
            if ($language == 'English') {
                $card = DB::connection('mtgjson')->table('cards')
                    ->where('name', $name)->whereIn('setCode', $possibleSetCodes)
                    ->first();
            } else {
                $card = DB::connection('mtgjson')->table('cards')
                    ->join('foreign_data', 'cards.uuid', 'foreign_data.uuid')
                    ->where('foreign_data.language', $language)
                    ->where('cards.name', $name)
                    ->whereIn('cards.setCode', $possibleSetCodes)
                    ->first();
            }

            if (! $card) {
                Log::warning("Couldn't find card '$name' with Set '$set'.", [
                    'item' => $item,
                ]);
                $errors[] = "Couldn't find card '$name' with Set '$set'.";

                // if special promo tappedout set identifier
                if ($set == '000') {
                    $card = (object)[
                        'name' => $name,
                        'multiverseId' => 0,
                        'setCode' => '000',
                        'scryfallId' => '000',
                    ];
                }

                // if translation wasn't found
                else if ($language != 'English') {
                    $card = DB::connection('mtgjson')->table('cards')
                        ->where('name', $name)->whereIn('setCode', $possibleSetCodes)
                        ->first();
                }
            }
            $set = $card->setCode;

            if ($card->multiverseId === null) {
                $errors[] = "No multiverse ID for card '$name' with Set '$set'.";
                $card->multiverseId = 0;
            }

            DB::table('inventory')->updateOrInsert([
                'printing' => $set,
                'condition' => $condition,
                'language' => $language,
                'foil' => $foil,
                'multiverseId' => $card->multiverseId,
                'scryfallId' => $card->scryfallId,
                'matchedName' => $card->name,
                'quantity' => $quantity,
            ], [
                'name' => $name,
            ]);
        }

        return back()->with('errors', $errors);
    }

    /**
     * @param array $matches
     *
     * @return array
     */
    private function getIdentifiers ($matches)
    {
        $idColumns = [5, 7, 9];
        $ids = [];
        foreach ($idColumns as $column) {
            if (isset($matches[$column])) {
                $ids[] = $matches[$column];
            }
        }

        return $ids;
    }

    /**
     * @param array $ids
     *
     * @return string
     */
    private function getFoil($ids)
    {
        $foils = array_keys(getTappedOutFoilMap());

        $foil = (array_values(array_intersect($ids, $foils)) ?: [''])[0];

        return getTappedOutFoilMap()[$foil];
    }

    /**
     * @param array $ids
     *
     * @return string
     */
    private function getLanguage ($ids)
    {
        $languages = array_keys(getTappedOutLanguageMap());

        $language = (array_values(array_intersect($ids, $languages)) ?: ['EN'])[0];

        return getTappedOutLanguageMap()[$language];
    }

    /**
     * @param array $ids
     *
     * @return string
     */
    private function getCondition($ids)
    {
        $conditions = array_keys(getTappedOutConditionMap());

        $condition = (array_values(array_intersect($ids, $conditions)) ?: ['NM'])[0];

        return getTappedOutConditionMap()[$condition];
    }
}
