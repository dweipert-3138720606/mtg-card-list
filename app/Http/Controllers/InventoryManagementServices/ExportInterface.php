<?php

namespace App\Http\Controllers\InventoryManagementServices;

use Illuminate\Http\Request;

interface ExportInterface
{
    public function export(Request $request);
}
