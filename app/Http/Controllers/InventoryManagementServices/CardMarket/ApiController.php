<?php

namespace App\Http\Controllers\InventoryManagementServices\CardMarket;

use App\Http\Controllers\Controller;
use CardmarketApi\OAuthMiddleware;
use GuzzleHttp\HandlerStack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\ArrayToXml\ArrayToXml;

class ApiController extends Controller
{
    public function __construct()
    {
        $stack = HandlerStack::create();
        $stack->push(new OAuthMiddleware(), 'oauth');

        $baseUrl = env('CARDMARKET_API_URL') ?: 'https://sandbox.cardmarket.com';
        $this->client = new Client([
            'handler' => $stack,
            'base_uri' => $baseUrl . '/ws/v2.0/output.json/',
            'cardmarket' => [
                'app_token' => env('CARDMARKET_APP_TOKEN'),
                'app_secret' => env('CARDMARKET_APP_SECRET'),
                'access_token' => env('CARDMARKET_ACCESS_TOKEN'),
                'access_token_secret' => env('CARDMARKET_ACCESS_TOKEN_SECRET'),
            ],
        ]);
    }

    public function export(Request $request)
    {
        $inventory = DB::table('inventory')->limit(50)->get();
        $errors = [];

        // first filter out all items from inventory, that can't be matched against a multiverseId in mtgjson
        $filteredInventory = [];
        foreach ($inventory as $card) {
            if ($card->language === 'English') {
                $mcmId = DB::connection('mtgjson')->table('cards')
                    ->where('multiverseId', $card->multiverseId)
                    ->value('mcmId');
            } else {
                $mcmId = DB::connection('mtgjson')
                    ->table('cards')->join('foreign_data', 'cards.uuid', 'foreign_data.uuid')
                    ->where('foreign_data.multiverseId', $card->multiverseId)
                    ->value('cards.mcmId');
            }

            if (empty($mcmId)) {
                $errors[] = "Couldn't find card '{$card->name}' with multiverseId '{$card->multiverseId}'";
                continue;
            }

            $card->mcmId = $mcmId;
            $filteredInventory[] = $card;
        }

        // check filtered inventory against stock.
        // build two arrays:
        // one array with cards IN the stock, so we just update the quantity
        // one array with cards NOT IN the stock, so we have to add them
        $stock = DB::table('mcm_stock')->get();
        $inStock = [];
        $notInStock = [];
        foreach ($filteredInventory as $card) {
            $stockItem = $stock->first(fn ($item) => $item->inventory_id == $card->id);

            if ($stockItem) {
                $card->article_id = $stockItem->article_id;
                $inStock[] = $card;
            } else {
                $notInStock[] = $card;
            }
        }

        // update cards in stock
        if (! empty($inStock)) {
            $inStockRequestBody = [];
            foreach ($inStock as $item) {
                $inStockRequestBody['article'][] = [
                    'idArticle' => $item->article_id,
                    'count' => $item->quantity,
                ];
            }
            $response = $this->client->request('PUT', 'stock', [
                'body' => ArrayToXml::convert($inStockRequestBody, 'request'),
            ]);
        }

        // add cards not in stock
        if (! empty($notInStock)) {
            $notInStockRequestBody = [];
            foreach ($notInStock as $item) {
                $notInStockRequestBody['article'][] = [
                    'idProduct' => $item->mcmId,
                    'idLanguage' => array_flip(getCardMarketLanguageMap())[$item->language],
                    'comments' => 'MTG Card List API',
                    'count' => $item->quantity,
                    'price' => '9999',
                    'condition' => $item->condition,
                    'isFoil' => $item->foil === 'foil' ? 'true' : 'false',
                    'isSigned' => $item->signed ? 'true' : 'false',
                    'isPlayset' => 'false',
                    'isAltered' => $item->alter ? 'true' : 'false',
                ];
            }
            $response = $this->client->request('POST', 'stock', [
                'body' => ArrayToXml::convert($notInStockRequestBody, 'request'),
            ]);
            $message = json_decode((string)$response->getBody());
            foreach ($message->inserted as $item) {
                if (isset($item->error)) {
                    $errors[] = $item->error . " - mcmId: '{$item->tried->idProduct}'";
                }
            }

            // update stock with new cards
            $this->updateStock($request);
        }

        // TODO: build csv file or smth with failed imports and filtered out items from above, so they can be added manually
        return back()->with('errors', $errors);
    }

    public function import(Request $request)
    {
        // TODO: set quantities from cardmarket
        // TODO: import cards not in inventory
    }

    public function updateStock(Request $request)
    {
        $stock = $this->client->getStock();
        DB::table('mcm_stock')->truncate();

        $success = DB::table('mcm_stock')->insert(array_map(fn ($item) => [
            'product_id' => $item->idProduct,
            'article_id' => $item->idArticle,
            'inventory_id' => DB::table('inventory')->where([ // find unique match
                'name' => $item->product->enName,
                'printing' =>
                    DB::connection('mtgjson')->table('set_translations')
                        ->where('translation', $item->product->expansion)
                        ->value('setCode') ?:
                    DB::connection('mtgjson')->table('sets')
                        ->where('mcmName', $item->product->expansion)
                        ->value('code'),
                'condition' => $item->condition,
                'language' => $item->language->languageName,
                'foil' => $item->isFoil ? 'foil' : '',
                'alter' => $item->isAltered,
                'signed' => $item->isSigned,
            ])->value('id'),
        ], $stock->article));

        if ($success) {
            return back()->with('success', 'Stock Updated!');
        } else {
            return back()->with('error', 'Stock update failed!');
        }
    }

    public function updatePrice()
    {
        // TODO: use local stock to iterate
        // TODO: check and set price for each
        $response = $this->client->request('GET', 'priceguide');
        dd((string)$response->getBody());
    }
}
