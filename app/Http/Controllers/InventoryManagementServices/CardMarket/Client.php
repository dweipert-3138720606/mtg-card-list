<?php

namespace App\Http\Controllers\InventoryManagementServices\CardMarket;

use Psr\Http\Message\ResponseInterface;

class Client extends \GuzzleHttp\Client
{
    public function getStockRaw($start = null): ResponseInterface
    {
        return $this->request('GET', 'stock', [
            'query' => [
                'start' => $start,
            ],
        ]);
    }

    public function getStock($start = null)
    {
        return json_decode((string)$this->getStockRaw($start)->getBody());
    }
}
