<?php

namespace App\Models\MTGJson;

class Model extends \Illuminate\Database\Eloquent\Model
{
    protected $connection = 'mtgjson';
}
