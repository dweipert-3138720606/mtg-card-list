<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    public string $multiverseId = '';
    public string $scryfallId = '';

    public string $name = '';

    public function getGathererImageUrl(): string
    {
        if (! $this->multiverseId) {
            return '/img/card-placeholder.jpg';
        }

        return "https://gatherer.wizards.com/Handlers/Image.ashx?multiverseid={$this->multiverseId}&type=card";
    }

    public function getScryfallImgUrl($size = 'normal'): string
    {
        if (! $this->scryfallId || $this->scryfallId === '000') {
            return '/img/card-placeholder.jpg';
        }

        $firstDigit = substr($this->scryfallId, 0, 1);
        $secondDigit = substr($this->scryfallId, 1, 1);

        return "https://img.scryfall.com/cards/$size/$firstDigit/$secondDigit/{$this->scryfallId}/front/.jpg";
    }

    public function isFoil()
    {
        return $this->foil === 'foil';
    }
}
